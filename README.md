# pitch_annotation

This scripts allows you to draw frequency contours with a mouse on spectrograms.
Typically this helps in analysing vocalizations with fundamental frequencies that are difficult to track with automatic procedures.

## use
The script loads a table called `data_for_pitch_DER.csv`.
In this table, each row denotes a vocalization, with a column `fn` for the path to the sound file, and `time` for its center position in the sound file (in seconds).

The interface allows you to go through each vocalization. After completing a vocalization, it saves a numpy pickle called `annot_pitch.npy`.
In this pickle, you will find an identifier for each vocalization, and its corresponding annotation as a binary matrix (the same size as the spectrogram) denoting annotated spectro-temporal bins.